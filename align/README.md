# Usage

Please execute all files from `discut` directory.

# Serialization directories

By default:
- anchors are saved in directory `saved_anchors` as `.npz` numpy files

- alignment matrices are saved in directory `saved_mappings` as `.pth` torch files

- parsed corpora are saved in directory `parsed_data` as `.npz` numpy files

- trained models are saved in directory `saved_models` as `.pth` torch files

# Required data

- MUSE bilingual dictionaries under **discut/dictionaries**:

`bash code/get_dictionaries.sh`

- for ELMo models: Glove embeddings under **discut/embeddings**:

`wget  -P embeddings/ "http://nlp.stanford.edu/data/glove.6B.zip"`
`cd embeddings`
`unzip glove.6B.zip`

# Compute alignment matrix

## `gen_alignment.py --src_corpus <src_corpus> --tgt_corpus <tgt_corpus>`
Compute a rotation matrix to closen BERT embeddings from a source language to those of a target language, using a bilingual dictionary. 

1. Compute anchors for both languages: given a monolingual dictionary, compute the mean vector representation of all words from the dictionary as they appear in a given corpus.

2. Solve the orthogonal Procrustes problem: given both anchor matrices, return W = argmin || WA - B || where W is an orthogonal matrix.

3. For analysis purposes: apply the transformation to the source embeddings. Compare distance between source and target vectors, before and after the transformation. 

Required arguments:

- `--src_corpus` and `--tgt_corpus`: names for source and target corpus, in DISRPT format (`lang.framework.corpus`)

Optional arguments:

- `--src_set` and `--tgt_set`: sets to use for anchor computation. Default is 'all' (concatenation of train, test and dev).

- `--src_format` and `--tgt_format`: `tok` (one instance = a batch of 510 words) or `conllu` (one instance = a sentence). Default: `conllu`.

- `--src_embs` and `--tgt_embs`: if anchors were pre-calculated and save as an .npz file, specify paths to skip directly to step 2.

- `--voc`: bilingual dictionary path. If not given, languages are inferred from corpora names and bilingual dictionary path is dictionaries/srclang-tgtlang.txt.

- `--show_examples`: number of word pair examples to show (distance computation before and after the transformation). Default: 3.

- `--plot_examples`: if the argument is passed, a few plots are produced and saved under images/ directory. 

## `gen_anchors.py --corpus <corpus> --voc <voc_file>`

Given a corpus and a monolingual vocabulary, compute mean BERT embedding for all words in the dictionary as they appear in the corpus.

Required arguments:

- `--corpus`

- `--voc`: a vocabulary file, with one token per line, including the `<UNK>` unknown token. Monolingual dictionaries may be computed from the MUSE bilingual dictionaries using `bil2mono.py dict.txt`.

Optional arguments:

- `--set` (train/test/dev/all) and `--format` (tok/conllu)

# Models

- `stupid_predictor.py <corpus_file>`: output a prediction for given corpus, where only the first token of each sentence is predicted to be a BeginSeg. The output file may then be evaluated like the others using `code/utils/seg_eval.py`. Predictions and scores are output in `result_stupid` directory.

- `train_model_baseline.py --corpus <corpus> --format <CONLLU/tok>`: train a model using a basic biLSTM on BERT embeddings.

- `train_model_rich.py <corpus>`: train a model using a biLSTM on BERT embeddings as well as embeddings for other annotation information (POS tag, distance to head, dependency relationship to 

- `test_model.py --model <model.pth> --format <CONLLU/tok> --type <BASELINE/rich> --corpus <test_corpus> --set <TEST/dev/train> --errors n --alignment <mapping.pth>`
Test a saved PyTorch baseline or rich model. `--errors` awaits an integers (default is 0) which is the number of examples of bad predictions to output on standard output.
If an alignment matrix is given (only for baseline model, for now), the rotation is applied right after the output of BERT embeddings.

# Utility files

- `bil2mono.py`: convert a MUSE bilingual dictionary to two monolingual dictionaries.

- `visualize.py`: functions to visualize results using Pyplot and PCA.

- `parse_corpus.py <corpus> [rich]`: parse a corpus and save useful information in `.npz` file (token ids, tokens, labels, + if rich: uposes, head ids, dependency relations). `<corpus>` must have the form `corpus_set.fmt` (where set is test/train/dev and fmt is tok/conllu).

- `disrpt2readable.py <file>`: convert a DISRPT RST or SDRT file to an easily readable text file, where segments are indicated by '|'. Text file is saved under the same directory with suffix `.readable`.
