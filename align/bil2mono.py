import os
import sys

def write_mono(output_file, words):
    with open(output_file, "w") as wf:
        for word in words:
            wf.write(word + '\n')
        wf.write('<UNK>\n')

def to_mono(dico):
    """From a bilingual dictionary file sr-tg.txt, output two monolingual dictionary files sr_sr-tg.txt and tg_sr-tg.txt in the same directory"""
    
    output_dir, dico_file = os.path.split(dico)
    beg_src = 0 
    end_src = 2 
    beg_tgt = 3
    end_tgt = 5
    output_src = os.path.join(output_dir, (dico_file[beg_src:end_src] + "_" + dico_file))
    output_tgt = os.path.join(output_dir, (dico_file[beg_tgt:end_tgt] + "_" + dico_file))

    if os.path.isfile(output_src) and os.path.isfile(output_tgt):
        print(f'Monolingual dictionaries {output_src} and {output_tgt} already exist.')
        return output_src, output_tgt

    words_src = set()
    words_tgt = set()

    with open(dico, "r") as rf:
        for line in rf:
            split = line.strip().split()
            if len(split) != 2: 
                print(line)
                print(split)
                print(f'Format error in {dico} file')
                sys.exit()
            words_src.add(split[0])
            words_tgt.add(split[1])
    write_mono(output_src, words_src)
    write_mono(output_tgt, words_tgt)
    print(f'Wrote monolingual dictionaries at {output_src} and {output_tgt}.')

    return output_src, output_tgt

def main():
    if len(sys.argv) != 2:
        print('usage: bil2mono.py dict.txt') 
        sys.exit()

    dico = sys.argv[1]
    to_mono(dico)

if __name__ == '__main__':
    main()

