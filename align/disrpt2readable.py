import sys, os
import pandas as pd
import numpy as np

from parse_corpus import parse

def readable(file_name):
    output_dir, input_file = os.path.split(file_name)
    output_file = file_name + '.readable'
    _, tokens, labels = parse(file_name, full_name=True, save=False)

    with open(output_file, "w") as f:
        for tok, lab in zip(tokens, labels):
            if lab: f.write('| ')
            f.write(f'{tok} ')
    print(f'Wrote file {output_file}.')

def main():
    if len(sys.argv) != 2:
        print("usage: disrpt2readable.py <file>")
        sys.exit()
    file_name = sys.argv[1]
    readable(file_name)

if __name__ == '__main__':
        main()
