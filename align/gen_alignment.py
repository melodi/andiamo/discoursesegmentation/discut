import torch
import numpy as np
import argparse, sys, os
from scipy.spatial.distance import cosine

from gen_anchors import anchors
from bil2mono import to_mono
from visualize import plot_heatmap, plot_rotation

def solve_procrustes(A, B):
    """Return W = argmin || WA - B || (Frobenius norm) where W is an orthogonal matrix"""
    A_t = A.transpose()
    M = np.matmul(B, A_t)
    U, sigma, Vh = np.linalg.svd(M)
    W = np.matmul(U, Vh)
    return W

def compute_distance(src, tgt, src_word, tgt_word, W_t):
    """Compute cosine distance between source word embedding and target word embedding, before and after alignment"""
    src_emb = src[src_word]
    tgt_emb = tgt[tgt_word]
    src_emb_al = np.matmul(src_emb, W_t)
    dist_before = cosine(src_emb, tgt_emb)
    dist_after = cosine(src_emb_al, tgt_emb)
    return dist_before, dist_after

def eval_alignment(aligning_pairs, src, tgt, W_t, show_examples):
    """Compute distance before and after alignment for word translation pairs and random word pairs.
    Print max_print examples to standard output"""
    l = len(aligning_pairs)
    align_transl = [0] * l
    align_non_transl = [0] * l

    if show_examples: print('** Translation pairs **')
    for i, (src_word, tgt_word) in enumerate(aligning_pairs):
        dist_before, dist_after = compute_distance(src, tgt, src_word, tgt_word, W_t)
        align_transl[i] = dist_before - dist_after
        if i < show_examples:
            print(f'{src_word} - {tgt_word}')
            print(f'Cosine distance before alignment: {dist_before}')
            print(f'Cosine distance after alignment: {dist_after}\n')
 
    if show_examples: print('** Non-translation pairs **')
    for i, (src_word, _) in enumerate(aligning_pairs):
        tgt_word = aligning_pairs[(i+10)%l][1]
        dist_before, dist_after = compute_distance(src, tgt, src_word, tgt_word, W_t)
        align_non_transl[i] = dist_before - dist_after
        if i < show_examples:
            print(f'{src_word} - {tgt_word}')
            print(f'Cosine distance before alignment: {dist_before}')
            print(f'Cosine distance after alignment: {dist_after}\n')

    print("Mean alignment for translation pairs", sum(align_transl) / l)
    print("Mean alignment for non-translation pairs", sum(align_non_transl) / l)

def compute_mapping(src_anchors, tgt_anchors, src_corpus, tgt_corpus, voc, show_examples):
    """
    src_anchors - path to .npz file storing a dictionary from tokens to anchors (source language)
    tgt_anchors - path to .npz file storing a dictionary from tokens to anchors (target language)
    ---
    Compute the best orthogonal mapping to align the source vectors to the target vectors"""

    output_dir = 'saved_mappings'
    output_file = os.path.join(output_dir, f'{src_corpus}_{tgt_corpus}.pth')
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    if os.path.isfile(output_file):
        print(f'Mapping already computed at {output_file}.')
        return output_file
    
    print(f'Starting computation of alignment matrix from {src_corpus} to {tgt_corpus}.')
    src = np.load(src_anchors, allow_pickle=True)
    src = src[src.files[0]][0]
    tgt = np.load(tgt_anchors, allow_pickle=True)
    tgt = tgt[tgt.files[0]][0]
    # collect word pairs for which we have anchors
    src_align = []
    tgt_align = []
    aligning_pairs = []
    
    with open(voc, "r") as f:
        for line in f:
            # a line has format 'src_word tgt_word'
            split = line.strip().split()
            if len(split) != 2:
                print(f'format error in {voc}')
                sys.exit()
            src_word, tgt_word = split
            if src_word in src.keys() and tgt_word in tgt.keys():
                src_align.append(src[src_word])
                tgt_align.append(tgt[tgt_word])
                aligning_pairs.append((src_word, tgt_word))

    A = np.array(src_align).transpose()
    B = np.array(tgt_align).transpose()
    W = solve_procrustes(A, B)
    torch.save(torch.from_numpy(W), output_file)
    print(f'Mapping saved at {output_file}.')
    W_t = W.transpose()
    eval_alignment(aligning_pairs, src, tgt, W_t, show_examples)
    return output_file

def get_dico(src_lang, tgt_lang):
    """
    Convert DISRPT language identifier to 2-character language identifier
    spa -> es
    tur -> tr
    others -> first two characters
    """
    convert = {'spa':'es', 'tur':'tr'}
    if src_lang in convert.keys():
        src_lang = convert[src_lang]
    elif len(src_lang) == 3:
        src_lang = src_lang[:2]
    if tgt_lang in convert.keys():
        tgt_lang = convert[tgt_lang]
    elif len(tgt_lang) == 3:
        tgt_lang = tgt_lang[:2]

    voc_file = os.path.join('dictionaries', f'{src_lang}-{tgt_lang}.txt')
    if not os.path.isfile(voc_file):
        print(f'File {voc_file} not found: either wrong path or this language configuration is unavailable.')
        sys.exit()
    return voc_file, src_lang, tgt_lang

def main():
    """Two uses:
    - pass as argument pre-computed anchors (.npz file)
    - pass as argument corpuses for which to compute anchors first (anchors are not recomputed if they already were). By default anchors are computed on the concatenation of train, test and dev sets in conllu format ('all' format)."""
    parser = argparse.ArgumentParser(description='Compute best rotation from source embeddings to target embeddings')
    parser.add_argument('--src_embs', help='embeddings to align (.npz)')
    parser.add_argument('--tgt_embs', help='embeddings to align to (.npz)')
    parser.add_argument('--src_corpus', required=True, help='corpus for which to compute anchors to align')
    parser.add_argument('--tgt_corpus', required=True, help='corpus for which to compute anchors to align to')
    parser.add_argument('--src_set', default='all', help='train/test/dev/all')
    parser.add_argument('--tgt_set', default='all', help='train/test/dev/all')
    parser.add_argument('--src_format', default='conllu', help='tok/conllu')
    parser.add_argument('--tgt_format', default='conllu', help='tok/conllu')
    parser.add_argument('--voc', help='bilingual dictionary for alignment (text file with two tokens per line separated by a space)')
    parser.add_argument('--show_examples', type=int, default=3, help='number of word pairs for which to print distance')
    parser.add_argument('--plot_examples', action='store_true', default=False, help='Plot a few word pairs for visualization. Available languages: de, en, fr (to add a language, modify common_words dictionary).')
    params = parser.parse_args()

    common_words = {'de':['ist','ein'],'en':['is','a'],'fr':['est','un']}

    src_lang = params.src_corpus.split('.')[0]
    tgt_lang = params.tgt_corpus.split('.')[0]
    if params.voc: 
        voc = params.voc
        _, src_lang, tgt_lang = get_dico(src_lang, tgt_lang)
    else:
        voc, src_lang, tgt_lang = get_dico(src_lang, tgt_lang)

    if params.src_embs and params.tgt_embs:
        src_embs = params.src_embs
        tgt_embs = params.tgt_embs
    else:
        src_voc, tgt_voc = to_mono(voc)
        if params.plot_examples and src_lang in common_words.keys() and tgt_lang in common_words.keys():
            src_words = common_words[src_lang]
            tgt_words = common_words[tgt_lang]
        else:
            src_words = []
            tgt_words = []
        src_embs, src_saved, src_sents = anchors(params.src_corpus, params.src_set, params.src_format, src_voc, save_words=src_words)
        tgt_embs, tgt_saved, tgt_sents = anchors(params.tgt_corpus, params.tgt_set, params.tgt_format, tgt_voc, save_words=tgt_words)

    mapping_file = compute_mapping(src_embs, tgt_embs, params.src_corpus, params.tgt_corpus, voc, params.show_examples)
    if params.plot_examples:
        W = torch.load(mapping_file)
        src_word = next(iter(src_saved.keys()))
        plot_heatmap(src_word, src_saved, params.src_corpus, src_sents)
        tgt_word = next(iter(tgt_saved.keys()))
        plot_heatmap(tgt_word, tgt_saved, params.tgt_corpus, tgt_sents)
        plot_rotation(src_saved, tgt_saved, W, params.src_corpus, params.tgt_corpus, plot_anchors=True)

if __name__ == '__main__':
    main()
