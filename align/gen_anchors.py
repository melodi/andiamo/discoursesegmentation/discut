import sys, os, argparse
import torch
import numpy as np
from transformers import BertModel
from torch.utils.data import DataLoader
from tqdm import tqdm
from train_model_baseline import generate_sentence_list, collate_batch
from allennlp.data import vocabulary

from visualize import plot_heatmap_anchors

bert = 'bert-base-multilingual-cased'

def save_embs(anchors, vocab, output_file, output_dir, save_txt=False): 
    #words not in the corpus are not included
    dico_anchors = {}
    for i, anchor in enumerate(anchors):
        if len(np.nonzero(anchor)[0]) > 0:
            token = vocab.get_token_from_index(i)
       	    dico_anchors[token] = anchor 

    nb_embs = len(list(dico_anchors.keys()))

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    if save_txt:
        with open((output_file[:-4] + '.txt'), "w") as wf:
            wf.write(str(nb_embs) + " " + '768\n')
            for token, anchor in dico_anchors.items():
                wf.write(token + ' ' + ' '.join([str(v) for v in anchor]) + '\n')

    np.savez_compressed(output_file, anchors = np.array([dico_anchors]))
    print(f"Anchors saved at {output_file}.")
    return output_file

def anchors(corpus, sset, fmt, vocab_file, save_words=[]):
    """
    corpus - corpus for which to compute anchors
    sset - train/test/dev
    fmt - conllu/tok
    vocab_file - path to a text file containing one token per line, including <UNK> unknown token
    save_words - a list of words for which to save contextual embeddings, for later visualization (optional)
    ---
    Given a corpus and a set of tokens, compute average contextual embedding representation for each token"""

    output_dir = 'saved_anchors'
    source = f'{corpus}_{sset}.{fmt}'
    output_file = os.path.join(output_dir, f'anchors_{source}.npz')
    if os.path.isfile(output_file) and not save_words:
        print(f'Anchors already computed at {output_file}.')
        return output_file, {}, {}
 
    print(f'Starting computation of anchors for corpus {source} from vocabulary file {vocab_file}.')
    vocab = vocabulary.Vocabulary()
    vocab.set_from_file(vocab_file, oov_token='<UNK>')
    oov_id = vocab.get_token_index(vocab._oov_token)
    anchors = torch.zeros((vocab.get_vocab_size(), 768), requires_grad=False)
    vocab_size = vocab.get_vocab_size()
    num_occurrences = [0] * vocab_size
    print(f'Loaded vocabulary of size {vocab_size} from file {vocab_file}.')
   
    if sset == 'all':
        ssets = ['train', 'test', 'dev']
        data = {}
        for s in ssets:
            data[s] = generate_sentence_list(corpus, s, fmt)
        data = data['train'] + data['test'] + data['dev'] #concatenate all sets
    else: 
        data = generate_sentence_list(corpus, sset, fmt)
    batch_size = 64
    
    dataloader = DataLoader(data, batch_size=batch_size, collate_fn=collate_batch)
    bert_embeddings = BertModel.from_pretrained(bert)

    #initialize dictionary to store embeddings for a few words (used for visualization afterwards)
    saved_embs = {}
    saved_sentences = {}
    for word in save_words:
        saved_embs[word] = []
        saved_sentences[word] = []

    #iterate through batches
    for sentence_batch in tqdm(dataloader):
        bert_output = bert_embeddings(**sentence_batch.getBatchEncoding()).last_hidden_state.detach().numpy()

        #iterate through sentences
        for i, sentence in enumerate(sentence_batch.tokens):
            bert_sentence_output = bert_output[i]
               
            #iterate through tokens
            for j, token in enumerate(sentence):
                bert_token_output = bert_sentence_output[j]
                if token in save_words:
                    saved_embs[token].append(bert_token_output) #.detach().numpy()) #save contextual embedding of token
                    saved_sentences[token].append(sentence) #save sentence context of token

                w_id = vocab.get_token_index(token)
                if w_id != oov_id:
                    n = num_occurrences[w_id]
                    anchors[w_id, :] = anchors[w_id, :] * (n / (n + 1)) + bert_token_output[:] / (n + 1)
                    num_occurrences[w_id] += 1

    anchors = anchors.detach().numpy()
    return save_embs(anchors, vocab, output_file, output_dir), saved_embs, saved_sentences

def main():
    parser = argparse.ArgumentParser(description='Compute anchors for a given dataset and a given word list')
    parser.add_argument('--corpus', required=True, help='corpus for which to compute anchors')
    parser.add_argument('--set', default='all', help='portion of the corpus to test on (train/test/dev/all)')
    parser.add_argument('--format', default='conllu', help='tok or conllu')
    parser.add_argument('--voc', required=True, help='words for which to compute anchors (a text file with one word per line and including <UNK> token)')
    parser.add_argument('--plot_examples', nargs='+', help='words for which to visualize anchors')
    params = parser.parse_args()

    vocab_file = params.voc
    if not os.path.isfile(vocab_file):
        print(f'no vocab file at {vocab_file}.')
        sys.exit()
    anchor_file, _, _ = anchors(params.corpus, params.set, params.format, vocab_file)
    anchs = np.load(anchor_file, allow_pickle=True)
    anchs = anchs[anchs.files[0]][0]
    if params.plot_examples:
        plot_heatmap_anchors(params.plot_examples, anchs, params.corpus)    

if __name__ == '__main__':
    main()
