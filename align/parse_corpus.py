import sys, os
import pandas as pd
import numpy as np

def parse(corpus, full_name=False, rich=False, save=True):
        """Parse RST or SDRT corpus to store:
        tok_ids - list of token ids (first column of tok/conllu file)
        tokens - list of tokens (2nd column)
        labels - last column, 1 if BeginSeg==Yes, 0 otherwise
        
        if rich:
            uposes - 4th column
            idheads - token id of the token's head, 7th column
            deprel - dependency relation between the token and its head, 8th column"""
        print(f'Parsing of {corpus} begins.')
        if not full_name:
            split = corpus.split('_')
            corpus_dir = split[0]
            input_file = os.path.join("data", corpus_dir, corpus)
        else: input_file = corpus
        output_dir = 'parsed_data'
        if rich:
            if not corpus.endswith('conllu'):
                print('Rich parsing only possible with conllu file')
                sys.exit()
            output_file = os.path.join(output_dir, f'parsedrich_{corpus}')
        else:
            output_file = os.path.join(output_dir, f'parsed_{corpus}')
        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)
        if os.path.isfile(output_file + '.npz') and save:
            print(f'{corpus} already parsed. do you wish to overwrite? (Y/n)')
            user = input()
            if not (user == "" or user == "Y" or user == "y"):
                print('done')
                sys.exit()

        column_names = ['tok_id','tok','lemma','upos','xpos','gram','idhead','deprel','type','label']
        df = pd.read_csv(input_file, names = column_names, skip_blank_lines=True, comment="#", sep="\t", quoting=3)
        tok_ids = df['tok_id'].values
        toks = df['tok'].values.astype(str)
        labels = df['label'].values.astype(str)
        if corpus.startswith('eng.rst.gum'):
            new_labels = np.zeros(labels.shape)
            for i, label in enumerate(labels):
                if isinstance(label, str) and "BeginSeg=Yes" in label:
                    new_labels[i] = 1
                else: new_labels[i] = 0
        else:
            new_labels = np.where((labels == "BeginSeg=Yes"), 1, 0) 
        labels = new_labels
        nb_segs = np.sum(labels)
       
        print(f'Done parsing.') 
        if rich:
            upos = df['upos'].values.astype(str)
            idheads = df['idhead'].values
            deprels = df['deprel'].values.astype(str)
            if save:
                np.savez_compressed(output_file, tok_ids = tok_ids, toks = toks, labels = labels, upos = upos, idheads = idheads, deprels = deprels)
                print(f'Data saved at {output_file}.')
            return tok_ids, toks, labels, upos, idheads, deprels

        else:
            if save:
                np.savez_compressed(output_file, tok_ids = tok_ids, toks = toks, labels = labels)
                print(f'Data saved at {output_file}.')
            return tok_ids, toks, labels
        

def main():
        if len(sys.argv) < 2 or len(sys.argv) > 3:
            print("usage: parse_corpus.py <corpus> [<rich>]")
            sys.exit()
        corpus = sys.argv[1]
        rich = False
        #rich = True means we collect not only token and label but also dependency relationship for conllu files
        if len(sys.argv) > 2:
            if sys.argv[2] == 'rich':
                rich = True
            else:
                print("usage: parse_corpus.py <corpus_path> [<rich>]")
                sys.exit()
        parse(corpus, rich=rich)

if __name__ == '__main__':
        main()
