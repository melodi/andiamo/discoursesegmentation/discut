import os, sys
import itertools

def is_divider(line):
    empty_line = line.strip() == ''
    if empty_line: return True
    else: 
        first_token = line.split()[0]
        if first_token == "#": return True
        else: return False

def stupid(input_file, output_file):

    with open(input_file, "r") as rf:
        with open(output_file, "w") as wf:
            for is_div, lines in itertools.groupby(rf, is_divider):
                if not is_div:
                    for i, line in enumerate(lines):
                        fields = line.strip().split('\t')
                        fields[2:-1] = ['_'] * len(fields[2:-1])
                        if i != 0:
                            fields[-1] = '_'
                        wline = '\t'.join(fields) + '\n'
                        wf.write(wline)

def main():
    output_dir = 'results_stupid'
    if len(sys.argv) != 2:
        print('usage: stupid_predictor.py <corpus_file>')
        sys.exit()
    input_file = sys.argv[1]
    input_dir, corpus_file = os.path.split(input_file)
    _, corpus_dir = os.path.split(input_dir)

    corpus_dir = 'results_' + corpus_dir
    output_dir = os.path.join(output_dir, corpus_dir)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    split = corpus_file.split('.')
    filename = '.'.join(split[:3]) + '.predictions.' + split[-1]
    output_file = os.path.join(output_dir, filename)

    stupid(input_file, output_file)
    
if __name__ == '__main__':
    main()

