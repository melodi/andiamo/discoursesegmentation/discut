import numpy as np
import sys
import os
import argparse
import torch
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm
from train_model_baseline import LSTM, SentenceBatch, generate_sentence_list, collate_batch
from train_model_rich import RichLSTM, generate_rich_sentence_list, collate_rich_batch

bert = 'bert-base-multilingual-cased'

def test(model_path, model_type, corpus, test_set, fmt, alignment, show_errors):
        model = torch.load(model_path)
        print(f'Model:\t{model_path}\nType:\t{model_type}\nEval:\t{corpus}_{test_set}\nFormat:\t{fmt}')

        if alignment:
            alignment = torch.load(alignment)
            model.loadAlignment(alignment)
        else: 
            model.alignment = None

        batch_size = 32
        if model_type == 'baseline':
            data = generate_sentence_list(corpus, test_set, fmt)
            dataloader = DataLoader(data, batch_size=batch_size, shuffle=True, collate_fn=collate_batch)
        else:
            data, _, _ = generate_rich_sentence_list(corpus, test_set, fmt)
            dataloader = DataLoader(data, batch_size=batch_size, shuffle=True, collate_fn=collate_rich_batch)

        model.eval()
        loss_fn = nn.BCELoss()
        
        l = len(dataloader.dataset)
        errors = []
        
        with torch.no_grad():
                total_acc, total_loss = 0, 0
                tp, fp, fn = 0, 0, 0
        
                for sentence_batch in tqdm(dataloader):
                        label_batch = sentence_batch.labels
                        if model_type == 'baseline':
                            pred = model(sentence_batch.getBatchEncoding())
                        else:
                            pred = model(sentence_batch.getBatchEncoding(), sentence_batch.upos, sentence_batch.deprels, sentence_batch.dheads)
                        loss = loss_fn(pred, label_batch)
                        pred_binary = (pred >= 0.5).float()
                        
                        for i in range(label_batch.size(0)):
                                for j in range(label_batch.size(1)):
                                        if pred_binary[i,j] == 1.:
                                                if label_batch[i,j] == 1.:
                                                        tp += 1
                                                else: fp += 1
                                        elif label_batch[i,j] == 1.: fn += 1
                        sum_score = ((pred_binary == label_batch).float().sum().item()) / label_batch.size(1)
                        assert (sum_score <= batch_size)
                        for i, sentence_id in enumerate(sentence_batch.sentence_ids):
                                if (pred_binary[i] != label_batch[i]).sum() > 0: #if there's at least one error
                                        errors.append((sentence_id, pred_binary[i]))
                        total_acc += sum_score
                        total_loss += loss.item() #*label_batch.size(0)

                precision = tp / (tp + fp) if (tp + fp != 0) else 'n/a'
                recall = tp / (tp + fn) if (tp + fn != 0) else 'n/a'
                f1 = 2 * (precision * recall) / (precision + recall) if (precision != 'n/a' and recall != 'n/a') else 'n/a'
    
        if show_errors > 0:
            print_errors(errors, data, max_print=show_errors)

        print(f"Acc\t{total_acc/l}\nLoss\t{total_loss/l}\nP\t{precision}\nR\t{recall}\nF1\t{f1}\n\n")

def print_segmentation(toks, labels):
        s = ""
        for i, tok in enumerate(toks):
                if i+1 < len(labels) and labels[i+1] == 1:
                        s += "| "
                s += str(tok) + " "
        print(s + '\n')

def print_errors(errors, data, max_print=25):
        print(f'Reporting {max_print} errors')
        max_print = min(max_print, len(errors))
        for sentence_id, pred in errors[:max_print]:
                _, (toks, labels) = data[sentence_id]
                print(f'Sentence {sentence_id}')
                print('Predicted:')
                print_segmentation(toks, pred)
                print('True:')
                labels = [0] + list(labels) + [0]
                print_segmentation(toks, labels)

def main():
        parser = argparse.ArgumentParser(description='Test a model on a dataset')
        parser.add_argument('--model', help='Path to .pth saved model')
        parser.add_argument('--format', default='conllu', help='tok or conllu')
        parser.add_argument('--type', default='baseline', help="baseline or rich model")
        parser.add_argument('--corpus', required=True, help='corpus to test on')
        parser.add_argument('--set', default='test', help='portion of the corpus to test on')
        parser.add_argument('--errors', type=int, default=0, help='number of prediction errors to display on standard output')
        parser.add_argument('--alignment', help='alignment matrix')

        params = parser.parse_args()

        if not os.path.isfile(params.model):
            print("model not found. please train the model first. please provide a relative path from discut dir.")
            sys.exit()
        if params.type == 'rich' and params.format == 'tok':
            print('a rich model requires a .conllu file')
            sys.exit()
        test(params.model, params.type, params.corpus, params.set, params.format, params.alignment, params.errors)

if __name__ == '__main__':
        main()
