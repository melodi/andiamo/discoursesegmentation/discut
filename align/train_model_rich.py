import numpy as np
import sys
import os
from transformers import BertModel
from transformers.tokenization_utils_base import BatchEncoding
import torch 
from torch import nn
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import DataLoader
from tqdm import tqdm

from train_model_baseline import SentenceBatch, add_cls_sep, toks_to_ids, make_labels, make_tok_types, make_tok_masks, save_model
from parse_corpus import parse

bert = 'bert-base-multilingual-cased'
#bert_embeddings = BertModel.from_pretrained(bert)

class RichLSTM(nn.Module):

        def __init__(self, batch_size, hidden_size, n_upos, upos_emb_dim, n_deprel, deprel_emb_dim, num_layers=1, bidirectional=False):
                super().__init__()
                self.batch_size = batch_size
                self.hidden_size = hidden_size

                #embedding dims 
                self.bert_emb_dim = 768
                self.upos_emb_dim = upos_emb_dim
                self.deprel_emb_dim = deprel_emb_dim

                #embedding layers
                self.bert_embeddings = BertModel.from_pretrained(bert)
                self.upos_embeddings = nn.Embedding(n_upos + 1, self.upos_emb_dim, padding_idx=0)
                self.deprel_embeddings = nn.Embedding(n_deprel + 1, self.deprel_emb_dim, padding_idx=0)
                lstm_input_size = self.bert_emb_dim + self.upos_emb_dim + self.deprel_emb_dim + 1
                self.lstm = nn.LSTM(lstm_input_size, hidden_size, num_layers, batch_first=True, bidirectional=bidirectional)
                d = 2 if bidirectional else 1
                self.hiddenToLabel = nn.Linear(d * hidden_size, 1)
                self.act = nn.Sigmoid()

                #convenient values for slicing the vectors: [ bert (768) | upos (U) | deprel (R) | dhead (1) ]
                self.bert_upos = self.bert_emb_dim + self.upos_emb_dim
                self.bert_upos_deprel = self.bert_emb_dim + self.upos_emb_dim + self.deprel_emb_dim

                self.alignment = None

        def forward(self, tok_batch, upos_batch, deprel_batch, dhead_batch):
                # batch: [B x L], where
                # B = batch_size
                # L = max sentence length in batch
                
                bert_output = self.bert_embeddings(**tok_batch)
                bert_output = bert_output.last_hidden_state
                if self.alignment is not None:
                    bert_output = torch.matmul(bert_output, self.alignment)
                # bert_output: [B x L x 768]
                upos_output = self.upos_embeddings(upos_batch)
                # upos_output: [B x L x U]
                deprel_output = self.deprel_embeddings(deprel_batch)
                # deprel_output: [B x L x R]
                dhead_output = dhead_batch[:,:,None]
                # dhead_output: [B x L x 1]
                #print("bert_output=", bert_output.shape)
                #print("upos_output=", upos_output.shape)
                #print("deprel_output", deprel_output.shape)
                #print("dhead_output", dhead_output.shape)

                full_output = torch.cat((bert_output, upos_output, deprel_output, dhead_output), dim=2)
                output64, (last_hidden_state, last_cell_state) = self.lstm(full_output)
                output1 = self.hiddenToLabel(output64)
                #print("output1=", output1.shape)
                return self.act(output1[:,:,0])

                #lstm_out, self.hidden = self.lstm(output, self.hidden)

        def loadAlignment(self, alignment):
                self.alignment = torch.transpose(alignment, 0, 1)

#class RichSentenceBatch():
#
#        def __init__(self, sentence_ids, tokens, tok_ids, tok_types, tok_masks, upos, deprels, dheads, labels):
#                self.sentence_ids = sentence_ids
#                self.tokens = tokens
#                self.tok_ids = pad_sequence(tok_ids, batch_first=True)
#                self.tok_types = pad_sequence(tok_types, batch_first=True)
#                self.tok_masks = pad_sequence(tok_masks, batch_first=True)
#                self.upos = pad_sequence(upos, batch_first=True)
#                self.deprels = pad_sequence(deprels, batch_first=True) #LOG SCALE
#                self.dheads = pad_sequence(dheads, batch_first=True)
#                self.labels = pad_sequence(labels, batch_first=True)
#
#        def getBatchEncoding(self):
#                dico = { 'input_ids': self.tok_ids, 'token_type_ids': self.tok_types, 'attention_mask': self.tok_masks }
#                return BatchEncoding(dico)
        
def cat_to_id(instance, cat):
        return np.where(cat == instance)[0][0] + 1

def dh_list(tok_ids, idheads, s, e):
        l = e - s
        ret = [0] * l 
        new_tok_ids = tok_ids[s:e]
        new_idheads = idheads[s:e]
        for i, idhead in enumerate(new_idheads):
            if idhead == 0:
                ret[i] = 0
            else:
                ret[i] = np.log(abs(idhead - new_tok_ids[i]))
        return ret

def generate_rich_sentence_list(corpus, sset, fmt):
        parsed_data = os.path.join("parsed_data", f"parsedrich_{corpus}_{sset}.{fmt}.npz")
        filename = f"{corpus}_{sset}.{fmt}"
        if not os.path.isfile(parsed_data):
            print("Parsed data not found.")
            tok_ids, toks, labels, upos, idheads, deprels = parse(filename, full_name=False, rich=True)
        else: 
            data = np.load(parsed_data, allow_pickle = True)
            tok_ids, toks, labels, upos, idheads, deprels = [data[f] for f in data.files]
        
        unique_upos = np.unique(upos)
        unique_deprels = np.unique(deprels)
        nb_upos = len(unique_upos)
        nb_deprels = len(unique_deprels)

        sentences = []

        previous_i = 0
        for index, tok_id in enumerate(tok_ids):
                if index > 0 and (tok_id == 1 or tok_id == '1' or (isinstance(tok_id, str) and tok_id.startswith('1-'))):
                        if index - previous_i <= 510:
                                sentences.append((toks[previous_i:index], [cat_to_id(u, unique_upos) for u in upos[previous_i:index]], [cat_to_id(d, unique_deprels) for d in deprels[previous_i:index]], dh_list(tok_ids, idheads, previous_i, index), labels[previous_i:index]))
                                previous_i = index
                        else: 
                                sep = previous_i + 510
                                sentences.append((toks[previous_i:sep], labels[previous_i:sep]))
                                if sep - previous_i > 510:
                                        print("still too long sentence...")
                                        sys.exit()
                                sentences.append((toks[sep:index], [cat_to_id(u, unique_upos) for u in upos[sep:index]], [cat_to_id(d, unique_deprels) for d in deprels[sep:index]], dh_list(tok_ids, idheads, sep, index), labels[sep:index]))

        indexed_sentences = [0] * len(sentences)
        for i, sentence in enumerate(sentences):
                indexed_sentences[i] = (i, sentence)

        return indexed_sentences, nb_upos, nb_deprels

def int_add_zeros(sentence):
        zero = torch.zeros(1).int()
        return torch.cat((zero, torch.tensor(sentence).int(), zero))

def add_zeros(sentence):
        zero = torch.zeros(1)
        return torch.cat((zero, torch.tensor(sentence), zero))

def collate_rich_batch(batch):
        sentence_ids, token_batch, upos_batch, idhead_batch, deprel_batch, label_batch = [i for i, (_, _, _, _, _) in batch], [j for _, (j, _, _, _, _) in batch], [k for _, (_, k, _, _, _) in batch], [l for _, (_, _, l, _, _) in batch], [m for _, (_, _, _, m, _) in batch], [n for _, (_, _, _, _, n) in batch]
        labels = [make_labels(sentence) for sentence in label_batch]
        tokens = [add_cls_sep(sentence) for sentence in token_batch]
        tok_ids = [toks_to_ids(sentence) for sentence in token_batch]
        lengths = [len(toks) for toks in tok_ids]
        tok_types = [make_tok_types(l) for l in lengths]
        tok_masks = [make_tok_masks(l) for l in lengths]
        uposes = [int_add_zeros(sentence) for sentence in upos_batch]
        deprels = [int_add_zeros(sentence) for sentence in deprel_batch]
        dheads = [add_zeros(sentence) for sentence in idhead_batch]
        
        return SentenceBatch(sentence_ids, tokens, tok_ids, tok_types, tok_masks, labels, uposes = uposes, deprels = deprels, dheads = dheads)
        
def train(corpus):
        print(f'starting rich training of {corpus}...')
        data, nb_upos, nb_deprels = generate_rich_sentence_list(corpus, 'train', 'conllu') 
        upos_emb_dim = int(np.sqrt(nb_upos))
        deprel_emb_dim = int(np.sqrt(nb_deprels))

        torch.manual_seed(1)

        #PARAMETERS
        batch_size = 8
        num_epochs = 6
        lr = 0.001
        reg = 0
        n_layers = 2
        n_hidden = 100
        dropout = 0.5
        grad_norm = 5
        bidirectional = True
        params = { 'fm' : 'conllu', 'bs': batch_size, 'ne': num_epochs, 'lr': lr, 'rg': reg, 'do': dropout, 'bi': bidirectional }

        dataloader = DataLoader(data, batch_size=batch_size, shuffle=True, collate_fn=collate_rich_batch)
        model = RichLSTM(batch_size, n_hidden, nb_upos, upos_emb_dim, nb_deprels, deprel_emb_dim, num_layers=n_layers, bidirectional=bidirectional) 
        loss_fn = nn.BCELoss() 
        optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=reg)
        model.train()
        
        l = len(dataloader.dataset)
        
        for epoch in range(num_epochs):
                total_acc = 0
                total_loss = 0
                tp = 0
                fp = 0 
                fn = 0
        
                for sentence_batch in tqdm(dataloader):
                        optimizer.zero_grad()
                        label_batch = sentence_batch.labels
                        #print("label_batch", label_batch.shape)
                        #print("tok_ids", sentence_batch.tok_ids.shape)
                        pred = model(sentence_batch.getBatchEncoding(), sentence_batch.uposes, sentence_batch.deprels, sentence_batch.dheads)
                        #print("pred", pred.shape)
                        loss = loss_fn(pred, label_batch)
                        loss.backward()
                        nn.utils.clip_grad_norm_(model.parameters(), grad_norm)
                        optimizer.step()
                        pred_binary = (pred >= 0.5).float()
                        
                        for i in range(label_batch.size(0)):
                                for j in range(label_batch.size(1)):
                                        if pred_binary[i,j] == 1.:
                                                if label_batch[i,j] == 1.:
                                                        tp += 1
                                                else: fp += 1
                                        elif label_batch[i,j] == 1.: fn += 1
                        #print("tp,fp,fn=",tp/label_batch.size(1),fp/label_batch.size(1),fn/label_batch.size(1))
                        #nb_1 = pred_binary.sum()
                        #print("nb predicted 1=", nb_1)
                        sum_score = ((pred_binary == label_batch).float().sum().item()) / label_batch.size(1)
                        assert (sum_score <= batch_size)
                        total_acc += sum_score
                        total_loss += loss.item() #*label_batch.size(0)

                precision = tp / (tp + fp) if (tp + fp != 0) else 'n/a'
                recall = tp / (tp + fn) if (tp + fn != 0) else 'n/a'
                f1 = 2 * (precision * recall) / (precision + recall) if (precision != 'n/a' and recall != 'n/a') else 'n/a'
                print(f"Epoch {epoch}\nAcc\t{total_acc/l}\nLoss\t{total_loss/l}\nP\t{precision}\nR\t{recall}\nF1\t{f1}\n")
     
        print('done training')
        output_file = save_model(model, 'rich', corpus, params)
        print(f'model saved at {output_file}')

def main():
        if len(sys.argv) != 2:
                print("usage: train_model_rich.py <corpus>")
                sys.exit()
        corpus = sys.argv[1]
        train(corpus) 

if __name__ == '__main__':
        main()
