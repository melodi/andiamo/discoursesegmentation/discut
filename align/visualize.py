import os
import torch
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt

import pickle

colors = ['c','m','g','r','b','y','k','w']
n_colors = len(colors)

output_dir = 'images'
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

def plot_heatmap_anchors(words, anchors, corpus, n_dim = 100, emb_dim = 768): 
    """
    words - a list of words for which to plot heatmap
    anchors - a dictionary from tokens to their anchor
    corpus - a string for naming the image file
    n_dim - number of dimensions to plot
    emb_dim - number of dimensions of an embedding
    ---
    Plot the first n_dim dimensions of a few word embeddings as a heatmap.
    """
    
    words = list(filter(lambda x: x in anchors.keys(), words))
    n = len(words)
    
    fig, axs = plt.subplots(nrows=1, ncols=n, subplot_kw={'xticks': [], 'yticks': []})

    for i, ax in enumerate(axs.flat):
        emb = anchors[words[i]].reshape(emb_dim,1)
        emb = emb[:n_dim,:]
        im = ax.imshow(emb, cmap='Blues', aspect='auto')
        ax.set_title(f'{words[i]}')

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    plt.show()
    file_name = f'heatmap_{corpus}_sample'
    plt.savefig(os.path.join(output_dir, file_name + '.png'))
    plt.clf()

def plot_heatmap(word, embeddings, corpus, sentences): 
    """
    word - a word for which to plot heatmap
    embeddings - a dictionary from tokens to a list of contextual embeddings
    sentences - a dictionary from tokens to the sentence their appeared in
    corpus - a string for naming the image file
    ---
    Plot the first n_dim dimensions of a few embeddings for a word as a heatmap.
    """
    
    embs = embeddings[word]
    sents = sentences[word]
    n_dim = 100
    n = 5
    if len(embs) > n: 
        embs = embs[:n]    
        sents = sents[:n]
    else:
        n = len(embs)
    
    fig, axs = plt.subplots(nrows=1, ncols=n, subplot_kw={'xticks': [], 'yticks': []})

    for i, ax in enumerate(axs.flat):
        emb = embs[i].reshape(768,1)
        emb = emb[:n_dim,:]
        im = ax.imshow(emb, cmap='Blues', aspect='auto')
        ax.set_title(f'({i+1})')

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    plt.show()
    file_name = f'heatmap_{corpus}_{word}'
    plt.savefig(os.path.join(output_dir, file_name + '.png'))
    plt.clf()

    with open(os.path.join(output_dir, file_name + '.txt'), "w") as f:
        f.write(f'>> {word} <<\n')
        for i, sent in enumerate(sents):
            sent = ' '.join(sent[1:-1])
            f.write(f'({i+1}). {sent}\n')

def plot_clouds(src_embeddings, tgt_embeddings=None, pca=None, plot_anchors=False, caption=None):
    """
    src_embeddings - a dictionary from tokens to a list of contextual embeddings
    tgt_embeddings - a second dictionary (optional)
    pca - already fit pca (optional)
    plot_anchors - whether to plot the mean of each token cloud (default: False)
    caption - saved image name
    ---
    Plot embeddings as points in a 2D space, using a color for each token.
    """

    #if no pca was given: first concatenate all embeddings as a single matrix, then fit PCA
    if not pca:
        full_embs = []
        for embs in src_embeddings.values():
            full_embs += embs
        if tgt_embeddings:
            for embs in tgt_embeddings.values():
                full_embs += embs
        full_embs = np.array(full_embs)
        pca = PCA(n_components=2).fit(full_embs)

    #go through embedding clouds, apply pca reduction, then plot
    for i, (word, embs) in enumerate(src_embeddings.items()):
        reduced = pca.transform(np.array(embs)).transpose()
        plt.scatter(reduced[0], reduced[1], color=colors[i%n_colors], label=word, s=1)
        if plot_anchors:
            anchor = np.array([np.mean(np.array(embs), axis=0)])
            reduced_anchor = pca.transform(anchor)[0]
            plt.plot(reduced_anchor[0], reduced_anchor[1], marker='X', color='k', ms=10)
    if tgt_embeddings:
        for i, (word, embs) in enumerate(tgt_embeddings.items()):
            reduced = pca.transform(np.array(embs)).transpose()
            plt.scatter(reduced[0], reduced[1], color=colors[i+len(src_embeddings)%n_colors], label=word, s=1)
            if plot_anchors:
                anchor = np.array([np.mean(np.array(embs), axis=0)])
                reduced_anchor = pca.transform(anchor)[0]
                plt.plot(reduced_anchor[0], reduced_anchor[1], marker='X', color='k', ms=10)

    plt.legend()
    output_file = f'contextual_embeddings_{caption}.png' if caption else 'contextual_embeddings.png'
    plt.savefig(os.path.join(output_dir, output_file))
    plt.show()
    plt.clf()

def plot_rotation(src_embeddings, tgt_embeddings, W, src_corpus, tgt_corpus, plot_anchors=False):
    """
    src_embeddings - a dictionary from tokens to a list of contextual embeddings (source language)
    tgt_embeddings - a dictionary from tokens to a list of contextual embeddings (target language)
    W - a rotation to apply to source embeddings
    src_corpus, tgt_corpus - strings for naming the image file
    plot_anchors - whether to plot the mean of each token cloud (default: False)
    ---
    Plot embeddings as points in a 2D space, before and after applying the rotation to the source embeddings.
    """
    W_t = torch.transpose(W, 0, 1)

    src_embeddings_aligned = {}
    for word, embs in src_embeddings.items():
        embs = np.array(embs)
        embs_al = np.matmul(embs, W_t).detach().numpy()
        src_embeddings_aligned[word] = list(embs_al)

    #fit pca
    full_embs = []
    for embs in src_embeddings.values():
        full_embs += embs
    for embs in tgt_embeddings.values():
        full_embs += embs
    for embs in src_embeddings_aligned.values():
        full_embs += embs
    full_embs = np.array(full_embs)
    pca = PCA(n_components=2).fit(full_embs)

    #plot before alignment
    plot_clouds(src_embeddings, tgt_embeddings, pca=pca, plot_anchors=plot_anchors, caption=f'before_{src_corpus}_{tgt_corpus}')
    #plot after alignment
    plot_clouds(src_embeddings_aligned, tgt_embeddings, pca=pca, plot_anchors=plot_anchors, caption=f'after_{src_corpus}_{tgt_corpus}')

