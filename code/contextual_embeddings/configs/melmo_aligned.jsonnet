// Configuration for the NER model with ELMo, modified slightly from
// the version included in "Deep Contextualized Word Representations",
// taken from AllenNLP examples
// modified for the disrpt discourse segmentation shared task -- 2019 
{

  "dataset_reader": {
    "type": "custom_conll_reader",
    "tag_label": "ner",
    "coding_scheme": "BIOUL",
    "token_indexers": {
      "tokens": {
        "type": "single_id",
        "lowercase_tokens": true
      },
      "token_characters": {
        "type": "characters",
        "min_padding_length": 3
      },
      "elmo": {
        "type": "elmo_characters"
     }
    }
  },
  "train_data_path": std.extVar("TRAIN_DATA_PATH"),
  "validation_data_path": std.extVar("TEST_A_PATH"),
  "model": {
    "type": "custom_simple_tagger",
    "text_field_embedder": {
      "token_embedders": {
        "tokens": {
            "type": "embedding",
            "embedding_dim": 50,
            "pretrained_file": "embeddings/glove.6B.50d.txt",
            "trainable": true
        },
	"elmo": {
	    "type": "elmo_token_embedder_multilang",
	    "do_layer_norm": false,
	    "dropout": 0.3,
	    "scalar_mix_parameters": [
		-9e10,
		1,
		-9e10
	    ],
	    "options_files": {
		"en": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/options262.json",
		"es": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/options262.json",
		"fr": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/options262.json",
		"it": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/options262.json",
		"pt": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/options262.json",
		"sv": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/options262.json",
		"de": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/options262.json"
	    },
	    "weight_files": {
		"en": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/en_weights.hdf5",
		"es": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/es_weights.hdf5",
		"fr": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/fr_weights.hdf5",
		"it": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/it_weights.hdf5",
		"pt": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/pt_weights.hdf5",
		"sv": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/sv_weights.hdf5",
		"de": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/de_weights.hdf5"
	    },
	    "aligning_files": {
			"en": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/en_best_mapping.pth",
                        "es": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/es_best_mapping.pth",
                        "fr": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/fr_best_mapping.pth",
                        "it": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/it_best_mapping.pth",
                        "pt": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/pt_best_mapping.pth",
                        "sv": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/sv_best_mapping.pth",
                        "de": "https://s3-us-west-2.amazonaws.com/allennlp/models/multilingual_elmo/de_best_mapping.pth"
            },
	},
       "token_characters": {
            "type": "character_encoding",
            "embedding": {
            "embedding_dim": 16
            },
            "encoder": {
            "type": "cnn",
            "embedding_dim": 16,
            "num_filters": 128,
            "ngram_filter_sizes": [3],
            "conv_layer_activation": "relu"
            }
        }
      }
    },
    "encoder": {
      "type": "lstm",
      "input_size": 1202,
      "hidden_size": 100,
      "num_layers": 1,
      "dropout": 0.5,
      "bidirectional": true
    },
    "regularizer": [
      [
        "scalar_parameters",
        {
          "type": "l2",
          "alpha": 0.1
        }
      ]
    ]
  },
  "iterator": {
    "type": "same_language",
    "batch_size": 2,
    "sorting_keys": [["words", "num_tokens"]],
    "instances_per_epoch": 32000
  },
  "trainer": {
    "optimizer": {
        "type": "adam",
        "lr": 0.001
    },
    //"validation_metric": "+f1_measure",
    "num_serialized_models_to_keep": 3,
    "num_epochs": 4,
    "grad_norm": 5.0,
    "patience": 2,
    "cuda_device": 0
  }
}
