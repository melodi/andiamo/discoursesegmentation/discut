# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
#

dl_path='https://dl.fbaipublicfiles.com/arrival'

lgs="af ar bg bn bs ca cs da de el en es et fa fi fr he hi hr hu id it ja ko lt lv mk ms nl no pl pt ro ru sk sl sq sv ta th tl tr uk vi zh"
mkdir -p dictionaries
for lg in ${lgs}
do
  for suffix in .txt .0-5000.txt .5000-6500.txt
  do
    fname=en-$lg$suffix
    curl -Lo dictionaries/$fname $dl_path/dictionaries/$fname
    fname=$lg-en$suffix
    curl -Lo dictionaries/$fname $dl_path/dictionaries/$fname
  done
done

# Download European dictionaries
for src_lg in de es fr it pt
do
  for tgt_lg in de es fr it pt
  do
    if [ $src_lg != $tgt_lg ]
    then
      for suffix in .txt .0-5000.txt .5000-6500.txt
      do
        fname=$src_lg-$tgt_lg$suffix
        curl -Lo dictionaries/$fname $dl_path/dictionaries/$fname
      done
    fi
  done
done
