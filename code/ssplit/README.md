# Requirements 

`ersatz 0.0.1` : `pip install ersatz==0.0.1`

# Usage

`python tok2conllu.py <file>` 

File must be a `.tok` file.

# Output

`<file>_conllu`: same file but sentences are separated by a line-jump and a commented line with sentence id.

# Ersatz library

Command-line usage:

`ersatz <input.txt> > <output.txt>`

Takes as input any text file and outputs the same text file with sentences separated by a line-break.


