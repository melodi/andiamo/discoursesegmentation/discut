To get scores, run (from `discut` dir):

`python code/utils/get_scores.py <result_dir> <output_file>`

This outputs a `.tsv` file summarizing all results up to this point collected in `result_dir`.

