import sys
import os
import pandas as pd

#Must be executed from discut dir

def parse_score(line):
	split = line.strip().split(": ")
	return str((round(float(split[-1]), 4) * 100))[:5]

def parse_model(line):
	#split = line.strip().split("_")
	model_ft = line[8:].strip().split("-")
	if len(model_ft) > 1: model = model_ft 
	else: model = [model_ft[0], "n/a"]
	return model

def parse_test(line):
	split = line.strip().split("_")
	return split[0]

def write_scores(result_dir, output_file):
	if not os.path.isdir(result_dir):
		print(f"directory {result_dir} doesn't exist.")

	_, dirs, _ = next(iter(os.walk(result_dir)))
	table = []
	
	for d in dirs:
		files = os.listdir(os.path.join(result_dir, d))
		for fil in files:
			if fil.endswith('test.scores'):
				with open(os.path.join(result_dir, d, fil), "r") as r_f:
					lines = r_f.readlines()
				if len(lines) >= 3:
					precision, recall, fscore = [parse_score(l) for l in lines[-3:]]
					model = parse_model(d)
					entry = [model[0], model[1], parse_test(fil), precision, recall, fscore]
					table.append(entry)

	columns = ['Model', 'Fine-tuning', 'Test', 'Precision', 'Recall', 'F1']
	df = pd.DataFrame(table, columns = columns)
	df.to_csv((output_file + ".tsv"), sep="\t", index=False)

def main():
	if len(sys.argv) != 3:
		print("usage: python get_scores.py <result_dir> <output_file>"); sys.exit()
	else:
		result_dir = sys.argv[1]
		output_file = sys.argv[2]
	write_scores(result_dir, output_file)

if __name__ == '__main__':
	main()
	
